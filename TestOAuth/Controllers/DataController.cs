﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace TestOAuth.Controllers
{
    public class DataController : ApiController
    {
       [AllowAnonymous]
       [HttpGet]
       [Route("api/data/formal")]
       public IHttpActionResult Get()
        {
            return Ok("Now server time is " + DateTime.Now.ToString());
        }
        [Authorize]
        [HttpGet]
        [Route("api/data/formal1")]
        public IHttpActionResult GetForAthenticate()
        {            
            return Ok("Hello");
        }
    }
}
